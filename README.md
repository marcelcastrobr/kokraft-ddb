# Konkraft Digital Design Basis #

Repository: https://bitbucket.org/marcelcastrobr/kokraft-ddb/src/master/

The purpose of this repository is to exchange information internally on TechnipFMC for the Kokraft DDB (Digital Design Basis) Project .

#### Folder structure:

- Ddbmodel: folder with ddb model developed. The source repository used for development is hold by IX3 on https://digitaldesignbasis.github.io/. 
- Subsea_studio: folder with the python script that modifies subsea studio configuration file according to ddb values.

# Digital Design Basis Goal:

![image-20210302084537805](README.assets/image-20210302084537805.png)

# Digital Design Basis Model:

#### Folder Structure:

- ddbmodel/ddb-use-case1.owl
- ddbmodel/ddb-use-case1-tpl.ttl
- ddbmodel/ddb-use-case1-inferred.ttl will have materialized inferred objectProperties (relations).
- ddbmodel/ddb-use-case1-tpl.blabel.ttl will have the blank nodes converted to namedIndividuals, so it can be replace the ddb-use-case1-tpl.ttl file.



#### Visualization:

- Please check kokraft-ddb[/src/master/ddbmodel/visualization/](/src/master/ddbmodel/visualization/ ) 



#### Additional project information:

- [WP2 on DDB Teams](https://teams.microsoft.com/_?tenantId=26b749f6-8c72-44e3-bbde-ae3de07b4206&login_hint=Marcel.CavalcantideCastro@technipfmc.com#/conversations/WP2%20-%20DDB%20Model%20development?threadId=19:58018fe2f6c3471fac15e98ae87c68b7@thread.skype&ctx=channel)

- [DDB Ix3 Webprotege](https://ddb.ix3.com/#projects/725f6d89-2548-4a7c-8698-fb836c423382/edit/Individuals?selection=NamedIndividual(%3Chttps://w3id.org/ddb/ddb-use-case1/UC1_Production_System%3E))



# Subsea Studio Integration

#### Integration architecture and steps.

![image-20210302084609988](README.assets/image-20210302084609988.png)



## DDB Parameters



| DDB Parameters                                               | Subsea Studio                               |
| ------------------------------------------------------------ | ------------------------------------------- |
| List of Components                                           | Tab file generated from list of components. |
| Surface roughness and thermal insulation for production pipeline | Imported in concept configuration           |
| Production profile for water, gas and oil flow rates         | Imported in concept configuration           |
| Water depth and ambient temperature                          | Imported in project configuration           |

## Lessons learned(draft):

- Units of measures -> No exposure of units of measures on subsea studio

- API as interface for better integration.

- Versioning the DDB model

- Size of model versus subsea studio needs 

  

#### Mapping script: 

- [data_extractor.py](https://bitbucket.org/marcelcastrobr/kokraft-ddb/src/master/subsea_studio/data_extractor.py) : main script responsible for changing the subsea studio concept json file and generate a modified json with ddb values.

- [sparql_queries.py](https://bitbucket.org/marcelcastrobr/kokraft-ddb/src/master/subsea_studio/sparql/sparql_queries.py): responsible for querying the DDB fuseki endpoint holding the ddb data.

  

How to run:

```python
python data_extractor.py -fin exportConcept.json -fout sample_mod2.json
```

Result:

Additional production profile is added to subsea studio as an additional year:

![image-20210302104409399](README.assets/image-20210302104409399.png)



## References

[1] Subsea studio application: https://subseastudio.apps.technipfmc.com/_login/

[2] Subsea studio maintenance mode: https://subseastudio.apps.technipfmc.com/#maintenance

[3] DDB Python script: https://bitbucket.org/marcelcastrobr/kokraft-ddb/src/master/subsea_studio/

[4] SPARQL tutorial: https://jena.apache.org/tutorials/sparql_basic_patterns.html

[5] [DDB Input APP Sketchup](https://www.sketch.com/s/81f556af-0111-491e-b7db-ed9e82d87e1a/a/3OO90Yw/play)

[6] [DDB Fuseki enpoint](https://fuseki.ddb.ix3.com/ddb)





# Presentation Layout

## DDB Goal

![image-20210302084537805](README.assets/image-20210302084537805.png)



## Field Layout

![image-20210312084925463](README.assets/image-20210312084925463.png)

## DDB Demo Application

![image-20210312085046155](README.assets/image-20210312085046155.png)

## DDB Usecase

![image-20210312084954072](README.assets/image-20210312084954072.png)





# Integration of DDB towards Subsea Studio

![image-20210312090211551](README.assets/image-20210312090211551.png)



# Results

![image-20210312092504605](README.assets/image-20210312092504605.png)