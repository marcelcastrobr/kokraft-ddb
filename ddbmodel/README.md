**File contents in the ddbmodel folder**


# ./  (Current folder)
## ddb_core.owl
The DDB ontology. Imports all other ontologies used in the use case

## ddb-use-case1.ttl
The use case model is created by applying templates to excel files, and manually modelling in Protege. This file contains the manual modelling. 

# target
Contains produced files. These should not be manually changed, but created with the Makefile

## ddb-use-case1-tpl.ttl 
The ontology containing the model elements created by OTTR templates applied to Excel sheets

## ddb-use-case1.ttl 
The combined triples of ddb-use-case1-tpl.ttl and ../ddb-use-case1.ttl. This means that this file contains the whole ddb use case 1 model. This also means that this file contains two Ontology individuals, which is not permitted as per the OWL specification. 

## ddb-use-case1.json
The same as the above, except in JSON-LD format. 

## ddb-use-case1-blabel.ttl
The same as the file above, except that all blank nodes has been skolemized and is in Turtle format. 


# jar
Contains java jar files

# sparql
sparql queries

# bottr
 tbd

# csv
Uncertain, possibly unused
