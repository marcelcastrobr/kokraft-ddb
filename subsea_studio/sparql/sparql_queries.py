#https://rebeccabilbro.github.io/sparql-from-python/

from SPARQLWrapper import SPARQLWrapper, JSON

# Specify the DBPedia endpoint
sparql = SPARQLWrapper("https://fuseki.ddb.ix3.com/ddb")

def water_depth_temperature_query(query_result):

    # Query for the description of "Capsaicin", filtered by language
    sparql.setQuery("""
        PREFIX rdfs:    <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX ddbc:    <https://w3id.org/ddb/ddb_core/>
        PREFIX uc1:     <https://w3id.org/ddb/ddb-use-case1/>
        PREFIX om:      <http://www.ontology-of-units-of-measure.org/resource/om-2/>
        PREFIX om2-ddb: <https://w3id.org/ddb/om-2-ddb/>
        PREFIX lis:     <http://standards.iso.org/iso/15926/part14/>
        
        select
        ?field ?depth_value ?depth_unit ?depth_unit_symbol
        ?ambient_temp_value ?ambient_temp_unit ?ambient_temp_unit_symbol
        {
        ?field ddbc:hasQuality [ a om:Depth ;
                                om:hasValue  [ a om:Measure ;
                                                om:hasNumericalValue  ?depth_value_literal ;
                                                om:hasUnit            ?depth_unit
                                                ]
                                ] ;
                ddbc:hasQuality [ a ddbc:AmbientTemperature ;
                                om:hasValue  [ a om:Measure ;
                                                om:hasNumericalValue  ?ambient_temp_value_literal ;
                                                om:hasUnit            ?ambient_temp_unit
                                                ]
                                ] .
        
        ?depth_unit om:symbol ?depth_unit_symbol .
        ?ambient_temp_unit om:symbol ?ambient_temp_unit_symbol .
        
        BIND ( STR(?depth_value_literal) as ?depth_value )
        BIND ( STR(?ambient_temp_value_literal) as ?ambient_temp_value )
        }
    """)

    # Convert results to JSON format
    sparql.setReturnFormat(JSON)
    result = sparql.query().convert()
    for hit in result["results"]["bindings"]:
        # We want the "value" attribute of the "comment" field
        query_result['waterDepth'] = hit["depth_value"]["value"]
        query_result['currentWaterDepth'] = hit["depth_value"]["value"]
        query_result['ambient_temp_value'] = hit["ambient_temp_value"]["value"]
        #print(hit["depth_value"]["value"])
        #print(hit["ambient_temp_value"]["value"])
    return query_result

def flow_rate_query(query_result):
    sparql.setQuery("""
    PREFIX rdfs:    <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX ddbc:    <https://w3id.org/ddb/ddb_core/>
    PREFIX uc1:     <https://w3id.org/ddb/ddb-use-case1/>
    PREFIX om:      <http://www.ontology-of-units-of-measure.org/resource/om-2/>
    PREFIX om2-ddb: <https://w3id.org/ddb/om-2-ddb/>
    PREFIX lis:     <http://standards.iso.org/iso/15926/part14/>
    
    select
    ?well ?well_lbl
    ?production_profile ?production_profile_lbl
        ?production_matter ?production_matter_lbl ?production_matter_ddbc_type
        ?production_matter_quantity ?production_matter_num_val ?production_matter_unit ?production_matter_unit_symbol
    {
    ?well a ddbc:Well ;
            rdfs:label ?well_lbl ;
            ddbc:hasProductionProfile ?production_profile .
    
    ?production_profile a ddbc:ProductionProfile-P50 ;
                                rdfs:label ?production_profile_lbl ;
                                ddbc:produces ?production_matter .
    
    ?production_matter a ?production_matter_ddbc_type ;
                        rdfs:label ?production_matter_lbl ;
                        ddbc:hasQuality [
                            a ?production_matter_quantity ;
                                om:hasValue [
                                om:hasNumericalValue ?production_matter_num_val_lit ;
                                om:hasUnit ?production_matter_unit
                            ]
                        ] .
    
    ?production_matter_ddbc_type rdfs:subClassOf+ lis:Compound .
    ?production_matter_unit om:symbol ?production_matter_unit_symbol .
    
    BIND ( STR(?production_matter_num_val_lit) as ?production_matter_num_val )
    }
    """)
    # Convert results to JSON format
    sparql.setReturnFormat(JSON)
    result = sparql.query().convert()

    for hit in result["results"]["bindings"]:
        # We want the "value" attribute of the "comment" field
        if hit["production_matter_ddbc_type"]["value"] == 'https://w3id.org/ddb/ddb_core/RawCruideOil':
            query_result['nominalOilFlow'] = float(hit["production_matter_num_val"]["value"])
            #print("nominalOilFlow: {}".format(hit["production_matter_num_val"]["value"]))
        elif hit["production_matter_ddbc_type"]["value"] == 'https://w3id.org/ddb/ddb_core/NaturalGas':
            query_result['nominalGasFlowRate'] = float(hit["production_matter_num_val"]["value"])
            #print("nominalGasFlowRate: {}".format(hit["production_matter_num_val"]["value"]))
        elif hit["production_matter_ddbc_type"]["value"] == 'https://w3id.org/ddb/ddb_core/ProducedWater':
            query_result['nominalWaterFlowRate'] = float(hit["production_matter_num_val"]["value"])
            #print("nominalWaterFlowRate: {}".format(hit["production_matter_num_val"]["value"]))
        else:
            print("Error: Query issue!!!")

    return query_result

def roughness_thermal_insulance_query(query_result):
    sparql.setQuery("""
    PREFIX rdfs:    <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX ddbc:    <https://w3id.org/ddb/ddb_core/>
    PREFIX uc1:     <https://w3id.org/ddb/ddb-use-case1/>
    PREFIX om:      <http://www.ontology-of-units-of-measure.org/resource/om-2/>
    PREFIX om2-ddb: <https://w3id.org/ddb/om-2-ddb/>
    PREFIX lis:     <http://standards.iso.org/iso/15926/part14/>
    
    select ?pipeline ?pipeline_label
    ?roughness_value ?roughness_unit ?roughness_unit_symbol
    ?thermal_insulance_value ?thermal_insulance_unit ?thermal_insulance_unit_symbol
    {
    ?pipeline rdfs:label ?pipeline_label ;
                ddbc:hasQuality [ a ddbc:SurfaceRoughness ;
                                om:hasValue  [ a om:Measure ;
                                            om:hasNumericalValue  ?roughness_value_literal ;
                                            om:hasUnit            ?roughness_unit
                                            ]
                                ] ;
                ddbc:hasQuality [ a om:ThermalInsulance ;
                                om:hasValue  [ a om:Measure ;
                                            om:hasNumericalValue  ?thermal_insulance_value_literal ;
                                            om:hasUnit            ?thermal_insulance_unit
                                            ]
                                ] .
    
    ?roughness_unit om:symbol ?roughness_unit_symbol .
    ?thermal_insulance_unit om:symbol ?thermal_insulance_unit_symbol .
    
    BIND ( STR(?roughness_value_literal) as ?roughness_value )
    BIND ( STR(?thermal_insulance_value_literal) as ?thermal_insulance_value )
    }
    """)
    # Convert results to JSON format
    sparql.setReturnFormat(JSON)
    result = sparql.query().convert()

    for hit in result["results"]["bindings"]:
        # We want the "value" attribute of the "comment" field
        query_result['Design_Uvalue'] = hit["thermal_insulance_value"]["value"]
        print("Design_Uvalue: {}".format(hit["thermal_insulance_value"]["value"]))
        query_result['Design_Roughness'] = hit["roughness_value"]["value"]
        print("Design_Roughness: {}".format(hit["roughness_value"]["value"]))
    return query_result

def component_query(query_result):
    sparql.setQuery("""
    """)
    # Convert results to JSON format
    sparql.setReturnFormat(JSON)
    result = sparql.query().convert()

    for hit in result["results"]["bindings"]:
        # We want the "value" attribute of the "comment" field
        query_result['roughness_value'] = hit["roughness_value"]["value"]
        print("roughness_value: {}".format(hit["roughness_value"]["value"]))
    return query_result

#print(result)
# The return data contains "bindings" (a list of dictionaries)
#result = water_depth_query()
ddb_dic = dict()
result = flow_rate_query(ddb_dic)
print(result['nominalOilFlow'])
    