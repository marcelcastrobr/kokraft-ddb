import json
import argparse
import sys
sys.path.append('./addict/addict')
from addict import Dict
from sparql import sparql_queries as _sparql
from nested_lookup import nested_lookup


def findfieldsiwant(obj,keyname):
    """
    Return value of a given key
    Arguments:
    - obj: dictionary
    - keyname: key from key:value pair
    Returns:
    - value of the key:value pair
    """
    for i in obj:
        if i.property == keyname:
            return i.value


def changefieldsiwant(obj,keyname,toadd):
    """
    Change value of given key:value pair
    Arguments:
    - obj: dictionary
    - keyname: key from key:value pair
    - toadd: value to be used
    Returns:
    - Nothing
    """
    for i in obj:
        if i.property == keyname:
            if isinstance(i.value, list):
                print("Appending: id {} : {} from {} to {} type {}".format(keyname,i.id, i.value,toadd, type(i.value)))
                i.value.append(toadd)
            else:
                print("Changing: id {} : {} from {} to {} type {}".format(keyname,i.id, i.value,toadd, type(i.value)))
                i.value = toadd
    return None

            

def fieldLayoutData_properties(j, ddb_dic):
    """
    Change key:values from given dictionary under fieldLayoutData_properties
    Arguments:
    - j: dictionary
    - ddb_dic: dictionary containing values from sparql query
    Returns:
    j: modified dictionary
    """
    list_fieldLayoutData_properties = []
    list_fieldLayoutData_properties.extend(["nominalOilFlow", "nominalGasFlowRate", "nominalWaterFlowRate", 
    "turndownPin", "duePoint", "wellHeadInjectionPressure", "Host_Injection_Temperature",
    "Host_Injection_Pressure", "year", "nominalTin", "nominalWHFP", "turndownOilFlow",
    "turndownWaterFlowRate", "turndownGasFlowRate", "turndownTin", "turndownWHFP", "topsidePressure",
    "Design_Uvalue", "Design_Roughness"])

    
    # Append a value in each list
    print("\n")
    for i in list_fieldLayoutData_properties:
        if i == 'year':
            new_year = findfieldsiwant(j.fieldLayoutData.properties,i)[-1]
            changefieldsiwant(j.fieldLayoutData.properties,i,new_year+1)
        elif i in ('nominalOilFlow', 'nominalGasFlowRate', 'nominalWaterFlowRate', 'Design_Uvalue', 'Design_Roughness'):
            changefieldsiwant(j.fieldLayoutData.properties,i,ddb_dic[i])
        else:
            if isinstance(findfieldsiwant(j.fieldLayoutData.properties,i), list):
                last_value = findfieldsiwant(j.fieldLayoutData.properties,i)[-1]
                changefieldsiwant(j.fieldLayoutData.properties,i,last_value)
            else:
                last_value = findfieldsiwant(j.fieldLayoutData.properties,i)
                changefieldsiwant(j.fieldLayoutData.properties,i,last_value)

    return j

def fieldLayoutData_nested_lookup(subseastudio_dictionay, ddb_dic):
    """
    Use nested lookup to change values in the given dictionary
    Arguments:
    - subseastudio_dictionay: dictionary
    - ddb_dic: dictionary containing values from sparql query
    Returns:
    subseastudio_dictionay: modified dictionary
    """
    list_fieldLayoutData = []
    list_fieldLayoutData.extend(["Design_Roughness"])
    #"waterDepth", "currentWaterDepth"])

    for my_key in list_fieldLayoutData:
        for i in nested_lookup(my_key, subseastudio_dictionay):
            if isinstance(i.value, list) & (my_key in ddb_dic):
                print("key: {} id: {}, type: {}, old_value: {} new_value: {}".format(my_key, i.id, type(i.value), i.value, ddb_dic[my_key]))
                i.value.pop()
                i.value.append(ddb_dic[my_key])
            elif (my_key in ddb_dic):
                print("key: {} id: {}, type: {}, old_value: {} new_value: {}".format(my_key, i.id, type(i.value), i.value, ddb_dic[my_key]))
                i.value = ddb_dic[my_key]
            #else:
            #    print("Key: {} -- Doing nothing".format(my_key))

    return subseastudio_dictionay

def write_result_to_file(subseastudio_dictionay, filename):
    """
    Write dictionary to file
    Arguments:
    - subseastudio_dictionay: dictionary
    Returns:
    - Nothing
    """
    with open(filename, 'w') as fout:
        json_dumps_str = json.dumps(subseastudio_dictionay, indent=4)
        fout.write(json_dumps_str)
    return None

def main():
    print("Lets change some values!!!")

    # Read in command-line parameters
    parser = argparse.ArgumentParser()
    parser.add_argument("-fin", "--jsonfilein", action="store", required=True, dest="jsonfilein", help="Json file to be parsed")
    parser.add_argument("-fout", "--jsonfileout", action="store", required=True, dest="jsonfileout", help="Modified Json file")
    args = parser.parse_args()
    filename_in = args.jsonfilein
    filename_out = args.jsonfileout

    # Query Sparql endpoint
    ddb_dic = dict()
    # Flow rate
    ddb_dic = _sparql.flow_rate_query(ddb_dic)
    ddb_dic = _sparql.water_depth_temperature_query(ddb_dic)
    ddb_dic = _sparql.roughness_thermal_insulance_query(ddb_dic)
    print(ddb_dic)

    # Read input file
    with open(filename_in, 'r') as f:
        datastore = json.load(f)

    # Convert input file to dictionary
    j = Dict(datastore)
    j = fieldLayoutData_properties(j, ddb_dic)
    j = fieldLayoutData_nested_lookup(j, ddb_dic)
    
    #Write dictionary to output file.
    write_result_to_file(j,filename_out)



if __name__ == "__main__":
    main()