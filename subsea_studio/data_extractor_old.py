import json
import argparse
import sys
sys.path.append('./addict/addict')
from addict import Dict

# Read in command-line parameters
parser = argparse.ArgumentParser()

parser.add_argument("-f", "--jsonfile", action="store", required=True, dest="jsonfile", help="Json file to be parsed")
args = parser.parse_args()
filename = args.jsonfile

#with open("./exportConceptSimplified.json", 'r') as f:
with open(filename, 'r') as f:
    datastore = json.load(f)

'''
sentences = []
labels = []
for item in datastore:
    sentences.append(item['version'])
    labels.append(item['conceptDTO'])
'''

j = Dict(datastore)
#print(j)
print("Manipulating file: {}".format(filename))
print("Pipeline Design Uvalue: {} (unit: {})".format(j.fieldLayoutData.pipelines[0].properties.Design_Uvalue.value, "-"))
print("Pipeline length: {} (unit: {})".format(j.fieldLayoutData.pipelines[0].properties.length.value, "-"))
print("waterDepth: {} (unit: {})".format(j.fieldLayoutData.hosts[0].properties.waterDepth.value, "-"))
print("defaultWaterDepth: {} (unit: {})".format(j.fieldLayoutData.pipelines[0].properties.defaultWaterDepth.value, "-"))
#print("WELL_CONTROL_Pressure_rating: {} (unit: {})".format(j.fieldLayoutData.equipments[9].properties.WELL_CONTROL_Pressure_rating.value, "-"))
#print("WELL_CONTROL_Max_Temperature_Rating: {} (unit: {})".format(j.fieldLayoutData.equipments[9].properties.WELL_CONTROL_Max_Temperature_Rating.value, "-"))

###
#- fieldLayoutData.reservoirs[0].properties
###
print("\n")
print("FROM: fieldLayoutData.reservoirs[0].properties")
#Gas Flow Rate
print("nominalGasFlowRate: {} (unit: {})".format(j.fieldLayoutData.reservoirs[0].properties.nominalGasFlowRate.value, "-"))
#Water Flow Rate
print("nominalWaterFlowRate: {} (unit: {})".format(j.fieldLayoutData.reservoirs[0].properties.nominalWaterFlowRate.value, "-"))
#Oil Flow Rate
print("nominalOilFlow: {} (unit: {})".format(j.fieldLayoutData.reservoirs[0].properties.nominalOilFlow.value, "-"))


###
#- fieldLayoutData.properties
###
print("\n\n")
print("FROM: fieldLayoutData.properties")
#assetSkills
nominalOilFlow = []
nominalGasFlowRate = []
nominalWaterFlowRate = []
nominalWHFP = []

for i in j.fieldLayoutData.properties:
    if i.property == 'nominalOilFlow':
        nominalOilFlow = i.value
    if i.property == 'nominalGasFlowRate':
        nominalGasFlowRate = i.value
    if i.property == 'nominalWaterFlowRate':
        nominalWaterFlowRate = i.value
    if i.property == 'nominalWHFP':
        nominalWHFP = i.value
    if i.property == 'nominalTin':
        nominalTin = i.value

#Gas Flow Rate
print("nominalGasFlowRate: {} (unit: {})".format(nominalGasFlowRate, "-"))
#Water Flow Rate
print("nominalWaterFlowRate: {} (unit: {})".format(nominalWaterFlowRate, "-"))
#Oil Flow Rate
print("nominalOilFlow: {} (unit: {})".format(nominalOilFlow, "-"))
#nominalWHFP
print("nominalWHFP: {} (unit: {})".format(nominalWHFP, "-"))
#nominalTin
print("nominalTin: {} (unit: {})".format(nominalTin, "-"))




#############
## Pipeline
#############
print("\n\n")
print("FROM: fieldLayoutData.pipelines")

Design_ID = []
Design_Uvalue = []
#Design_ID
for i in j.fieldLayoutData.pipelines:
    Design_ID = i.properties.Design_ID.value
    Design_Uvalue = i.properties.Design_Uvalue.value

#Design_ID
print("Pipeline Design_ID: {} (unit: {})".format(Design_ID, "-"))
print("Pipeline Design_Uvalue: {} (unit: {})".format(Design_Uvalue, "-"))